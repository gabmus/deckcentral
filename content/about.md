---
title: 'About'
nosearch: true
norss: true
showDate: false
---

Hi, my name is Gabriele Musco, I am a developer and Linux enthusiast.

I always believed that Linux was the future of desktop computing, and Valve seems to think the same, with their continued support of the Linux desktop, and with the recent release of the Steam Deck.

This website is a collection of tips, tricks, tutorials and any other knowledge that Steam Deck users, as well as new desktop users, will find useful, not just for gaming, but for general computing as well.

Although I don't own a Steam Deck myself (yet?), I've been using Linux exclusively on the desktop for 10+ years, and hopefully my knowledge, combined with the power of the community, can help new Deck users make the best out of their new system.

[Follow Deck Central on Mastodon](https://mastodon.social/deckcentral)

You can find out more about me [on my personal website](https://gabmus.org).

To view this website's source code, and to contribute to it, you can visit the [GitLab repository](https://gitlab.com/gabmus/deckcentral).

For business inquiries, write to gabmus at disroot dot org.

Feel free to [reach out on the Fediverse](https://linuxrocks.online/@gabmus).
