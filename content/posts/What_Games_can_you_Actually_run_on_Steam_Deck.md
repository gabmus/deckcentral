---
title: "ProtonDB: What Games Can You Actually Run on the Steam Deck?"
date: 2022-03-15T13:40:44+01:00
tags:
  - compatibility
  - proton
  - protondb
  - anticheat
image: "/posts/pics/What_Games_can_you_Actually_run_on_Steam_Deck/cover.webp"
---

As we all know the Steam Deck runs Linux, which is amazing! But at the same time most games out there are only made to work on Windows.

This problem has largely been addressed thanks to Proton. Wonder what Proton is? Read the beginning of [this article I made about Proton GE](/posts/how_to_setup_proton_ge/) to learn more.

Although Proton is great, and probably one of the biggest advancements in Linux gaming, it's not perfect.

As you might have found by yourself, there are still some games out there that don't play too well with Proton, or that just outright refuse to work. If you're lucky, it will be possible to fix some of these games with a few manual tweaks.

Today we'll explore [**ProtonDB**](https://www.protondb.com), a websites that will help you figure out if a game is compatible, and if it needs any tweaks to run properly.

![](/posts/pics/What_Games_can_you_Actually_run_on_Steam_Deck/protondb_1.png)

Opening up the home page, you'll immediately see a very useful overview of what the Linux gaming scene looks like, with various indicators of compatibility, including _Deck Verified_, _Medals_ and _Click Play_. Let's see what they actually mean.

#### Deck Verified

This pertains to data that's coming directly from Valve's Deck Verified program. This means that this indicator is at the same time the most reliable and the one that has the least data. These are games that Valve themselves deem either Verified on Deck, Playable with some caveats (like lack of controller support or small text on the Deck screen), or Unsupported, meaning they aren't deemed playable on the Steam Deck.

A quick note about _Unsupported_ games: a number of them are marked unsupported because they specifically won't run _on the Steam Deck_, but might otherwise run on a traditional Linux PC. For example, this includes any VR game, even if it works on Linux, but since the Deck doesn't officially support playing VR games they end up in this category.

#### Click Play

This is a recent addition to ProtonDB, and it's an indicator of how and if the game runs by just installing it and clicking Play. In other words, **how the game plays without any tinkering**.

This rating system has 5 tiers, from 1 being the best to 5 being the worst. Here are their descriptions right from ProtonDB:

- **Tier 1**: The highest tier. Ubiquitously positive reports.
- **Tier 2**: The second-highest tier. Some reports of trouble but is mostly good.
- **Tier 3**: Middle tier. A mix of positive and negative.
- **Tier 4**: Second-lowest tier. Negative reports overall.
- **Tier 5**: Lowest tier. Borked!

This new rating system is pretty straight forward and in my opinion it's your best bet if you don't want to tinker with games in order to play them.

#### Medals

This is the old rating system ProtonDB has always used since its inception and it's a descendant of Wine's rating system (this really brings me back! We didn't always have Proton, or even Steam on Linux for a very long time, and the WineHQ compatibility reports were what we used back then).

This rating system is a little more complicated, and it takes tinkering into consideration. Here's how ProtonDB describes the various levels, from best to worst:

- **Platinum**: Runs perfectly out of the box
- **Gold**: Runs perfectly after tweaks
- **Silver**: Runs with minor issues, but generally is playable
- **Bronze**: Runs, but often crashes or has issues preventing from playing comfortably
- **Borked**: Either won't start or is crucially unplayable

---

Now that you're an expert on rating systems, let's look for some games, and see how and if they will work!

On the top left corner of the website you can find a very convenient search bar. Let's search for a game, for example _Counter-Strike: Global Offensive_.

![](/posts/pics/What_Games_can_you_Actually_run_on_Steam_Deck/protondb_csgo_search.png)

There it is, it says _Native_. Native games are built specifically to run on Linux systems, so they're even better (mostly) than Platinum, as they won't even need Proton to work.

Let's click on it to read more.

![](/posts/pics/What_Games_can_you_Actually_run_on_Steam_Deck/protondb_csgo.png)

As you can see, even native games aren't perfect. CS:GO is a great example of this, since it needs a couple of launch options to work on some systems. Of course your mileage may vary, but if you're getting crashes or weird issues, try to follow the steps provided in the compatibility reports.

Ok, let's try with another game, say _Hades_.

![](/posts/pics/What_Games_can_you_Actually_run_on_Steam_Deck/protondb_hades_search.png)

Platinum, that's a great sign! Let's click on it and see if there's anything else we need to know.

![](/posts/pics/What_Games_can_you_Actually_run_on_Steam_Deck/protondb_hades.png)

Doesn't seem like it! Everyone seems to report that it just works (and I can confirm, plus Hades is a great game).

Let's move on, this time let's go with _Skyrim Special Edition_.

![](/posts/pics/What_Games_can_you_Actually_run_on_Steam_Deck/protondb_skyrim_search.png)

That's Gold. A good sign, but let's see why it's like that, and what we can do to fix it.

![](/posts/pics/What_Games_can_you_Actually_run_on_Steam_Deck/protondb_skyrim.png)

Apparently there are some issues with NPC voices, but it seems that the solution is simply adding some launch options. That's easy enough, so it should be good to go!

Now, let's search for something _the cool kids_ are playing, say _PUBG_.

![](/posts/pics/What_Games_can_you_Actually_run_on_Steam_Deck/protondb_pubg_search.png)

That's marked as borked. It means it won't work. Just out of curiosity, let's see why that is.

![](/posts/pics/What_Games_can_you_Actually_run_on_Steam_Deck/protondb_pubg.png)

And right there is the reason. PUBG uses the BattleEye anti-cheat system, but for some reason they haven't enabled Linux support for it (the process to enable Linux support consists in sending an e-mail and letting BattleEye do the rest).

Well, that's too bad. Fortunately there are lots of other great games you can play on Linux and the Steam Deck. In particular if you're looking for a Battle Royale kind of game, I suggest you check out Apex Legends: it works great and [it's been recently marked as Steam Deck Verified](https://www.gamingonlinux.com/2022/03/apex-legends-gets-steam-deck-verified/).

Let's move on with a game that recently landed on PC: _Final Fantasy VII Remake_.

Wait, wait. I hear you saying: _"ehrm... it's not there"_. Yes, you're correct: you won't find it on ProtonDB. That's because you won't even find it on Steam, since it's (still?) an Epic Games Store exclusive on PC. ProtonDB collects compatibility reports for Steam games, but not from other sites.

Unfortunately there isn't an up to date central resource for non-Steam games like ProtonDB, so you're probably better off just searching for _"name of your game linux"_ on a regular search engine (for what it's worth, I've heard that Final Fantasy VII Remake works great on Linux).

For games from GOG or the Epic Games Store, once you know they're compatible, [you can easily install them by using the Heroic Games Launcher](/posts/install_games_from_epic_game_store_and_gog_with_heroic_games_launcher/).

And that's it for today! Hopefully you'll find this little guide useful. Let me know what you think, and ask any question you have down in the comments.
