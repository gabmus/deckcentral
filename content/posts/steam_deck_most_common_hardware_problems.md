---
title: "Steam Deck Most Common Hardware Problems"
date: 2022-04-03T12:49:27+02:00
tags:
  - hardware
  - issue
  - problem
  - defect
  - joystick
  - drift
  - ips
  - glow
  - backlight
  - bleed
  - button
  - stuck
  - unresponsive
image: "/posts/pics/steam_deck_most_common_hardware_problems/cover.webp"
---

_Disclaimer: this article is for informative purposes only. I am not responsible for anything happening to you, your Steam Deck or anything and anyone else. Do NOT try to open your Steam Deck and repair it on your own, instead get in contact with Valve or a proper repair shop. Mishandling electronics can lead to electrical shock, dangerous lithium fires and in extreme cases even death._

---

Inevitably, all hardware products can have defects. It's a consequence of mass production: given how many units of one produict a company makes, some defects are bound to happen.

Of course, if you encounter such an issue, you can return your Steam Deck and get a new one, so it's not really too big of a problem, except for the fact that you'd have to wait for the replacement to come. It can be an annoyance but at least you will get a properly functioning unit.

Let's see what kind of hardware problems you can expect, as reported by users.

## Stuck B face button

{{< figure
  src="/posts/pics/steam_deck_most_common_hardware_problems/stuck_b.webp"
>}}

This seems to be one of the most common problems. It seems like Valve encountered this particular problem in their prototypes and were confident to have fixed it in the retail units, but unfortunately it doesn't seem like it.

Some users seem to have it worse than others, with a few saying that it got better for them over time.

I personally suggest you to **get your Steam Deck replaced** if you encounter this particular issue.

## Stuck or unresponsive power button

{{< figure
  src="/posts/pics/steam_deck_most_common_hardware_problems/power_button.jpg"
  attr="Frame extracted from iFixit's Steam Deck teardown video"
  attrlink="https://www.ifixit.com/Teardown/Steam+Deck+Teardown/148714"
>}}

This is a bit more serious and easier to miss, since the power button doesn't see as much use as one of the face buttons.

The type of switch used for the power button is different from the one used in the face buttons. This particular type of switch can be defective in such a way that you can feel and hear the click, but the press doesn't get registered.

Affected users report that this issue gets worse over time, and it corresponds with my personal experience with this type of switch.

Not being able to power on your Steam Deck because of a faulty button is incredibly frustrating.

If you have this particular problem **get your Steam Deck replaced** while you still can.

If you're reading this article in the future, and your device is out of warranty, chances are that you can get it repaired fairly easily, since this kind of switch is a reasonably standard part. Get in contact with a repair shop that you trust and that is competent in soldering and desoldering small parts.

## Joystick drift

{{<
  video
  h264="/posts/pics/steam_deck_most_common_hardware_problems/joystick_drift.mp4"
  h265="/posts/pics/steam_deck_most_common_hardware_problems/joystick_drift_h265.mp4"
  vp9="/posts/pics/steam_deck_most_common_hardware_problems/joystick_drift_vp9.webm"
  vp8="/posts/pics/steam_deck_most_common_hardware_problems/joystick_drift_vp8.webm"
  attr="Video by reddit user u/Stijnnl"
  attrlink="https://www.reddit.com/r/SteamDeck/comments/t49415/unfortunately_im_already_experiencing_stick_drift/"
>}}

This is another common problem. It happens with every joystick, on any platform including the Steam Deck, Nintendo Switch, Play Station and XBOX controllers.

The way it presents itself is having your character or cursor move in games without touching your joystick.

[Wikipedia does a great job at explaining joystick drift](https://en.wikipedia.org/wiki/Analog_stick#Neutral_position_and_drifting):

> To operate properly, an analog stick must establish a neutral position, a special, unique position which the stick must maintain that the controller would interpret as an intentional cessation or absence of in-game movement. Ideally, this would be the stick's very center when it is not touched or moved. Whenever the controller is activated or the system it is connected to is powered on, the current position of its analog stick(s) become the established neutral position. If the analog stick is moved away from its center during a time while it is established, the neutral position would shift to some place away from the center of the stick, causing the controller to interpret the center motionless position of the stick as in-game movement, since it is not the neutral position as it should be. This phenomenon, commonly called _drifting_, causes undesired gameplay effects, depending on the current game's controls, such as constant movement of the player character in a single direction or the game camera being skewed towards one particular angle while the affected stick is unmoved, and can only be corrected by performing particular actions that would restore the affected analog stick's neutral position back to the center of the analog stick.

You can experience this problem either immediately, or after some time as the joystick naturally wears with use.

To some degree, this problem can be fixed with software calibration, but depending on the severity it might not be enough.

If your Deck is under warranty, I suggest you to **get a replacement or have it repaired by Valve**. If it's not under warranty, it's still a very easy fix.

Both of the Steam Deck joysticks are separate daughter boards that can be easily replaced with new parts by a professional repair shop.

This was a very well thought out choice by Valve, as joystick drift is very common, and some could say even inevitable, so having the most likely point of failure in the device be easily replaceable is a great move to ensure its longevity throughout the years.

## IPS glow or backlight bleed

{{< figure
  src="/posts/pics/steam_deck_most_common_hardware_problems/backlight_bleed.jpg"
  attr="Photo by flickr user Shepard4711 (CC-BY-SA)"
  attrlink="https://www.flickr.com/photos/shepard4711/8238692078/"
>}}

This is another very common problem. IPS screens have this tendency of bleeding out light from the sides. This is again a very common occurrence in any kind of IPS display, from smartphones to desktop monitors and TVs and even portable consoles like the Steam Deck or the Nintendo Switch.

It's most noticeable in darker scenes, or when showing a black image on screen.

Unfortunately there isn't much you can do about this one, and IPS glow isn't even really considered as an issue by most manufacturers.

You can try to get your unit replaced by Valve, but you can't have any guarantee that the new unit you'd receive would be unaffected by this.

If it's not too bad, my suggestion is to **just ignore it**. It's probably not what you wanted to hear, but I'm afraid there isn't much to do for this one. The good thing is that this problem likely won't get any worse over time, as it's inherent to how IPS screens are produced.

---

That's all I could find about Steam Deck hardware problems. If you have experienced some other kind of issue you'd like to report, feel free to leave a comment below: I'll do my best to document the issue and add it to the article.
