---
title: "Install Games From Epic Game Store and GOG with Heroic Games Launcher"
date: 2022-03-11T18:05:00+01:00
image: "/posts/pics/Install_Games_from_Epic_Game_Store_and_GOG_with_Heroic_Games_Launcher/cover.webp"
featured: true
tags:
  - epic
  - gog
  - flatpak
  - app
  - heroic
  - legendary
  - proton
  - wine
---

Keeping on the theme of using non-Steam games, today I'll tell you about the [Heroic Games Launcher](https://heroicgameslauncher.com/).

Heroic is a community made Game Launcher that allows you to download, manage and play games you might have bought from [GOG](https://gog.com) or the [Epic Game Store](https://www.epicgames.com/store).

This particular tool felt necessary since neither GOG nor Epic have a Linux client to speak of. With GOG you can just download the games you bought from their website, and then just install them manually, but as far as I know, with the Epic store you don't have any choice (officially at least) but to use their Windows only client.

As for the Epic Store, that's where the [Legendary project](https://github.com/derrod/legendary), in combination with Heroic, come into play.

Quoting from Legendary's readme file:

> Legendary is an open-source game launcher that can download and install games from the Epic Games platform

Heroic is built on top of Legendary, providing a familiar user interface on top of Legendary, plus some extra bonuses.

Support for GOG in Heroic is a fairly recent addition, and while it's not 100% necessary to use GOG games on Linux and the Steam Deck, it makes the whole experience easier and more streamlined.

### Install Heroic

Installing Heroic has recently been made a lot easier: you can just install it using Flatpak.

Go to desktop mode, open up the **Discover** app center, search for **Heroic Games Launcher** and install it.

Alternatively you can install it from the terminal with the following command:

```bash
flatpak install com.heroicgameslauncher.hgl
```

### How to use it

The first time you launch Heroic, you'll be prompted to log into either your Epic Games or GOG account.

![](/posts/pics/Install_Games_from_Epic_Game_Store_and_GOG_with_Heroic_Games_Launcher/heroic_login.png)

You'll be able to manage your accounts in the future by going to your username on the bottom right corner of the window and selecting _Manage Accounts_, as shown here:

![](/posts/pics/Install_Games_from_Epic_Game_Store_and_GOG_with_Heroic_Games_Launcher/heroic_manage_accounts.png)

This page will allow you to log into another one of the two services offered, or log out.

Before continuing on with installing games, you probably want to have a look at the _Wine Manager_ section.

As I've explained in [my previous article about Proton GE](/posts/how_to_setup_proton_ge), Wine is the compatibility layer that allows you to run Windows only games and programs on Linux and the Steam Deck.

There are many different versions of Wine and Proton (Proton being a _distribution_ of Wine originally made by Valve, with extra features and configurations tailored towards gaming), and Heroic offers you a dedicated section to install a variety of them.

In Heroic, go ahead and select _Wine Manager_ on the sidebar. You'll be presented with a long list of different flavors of Wine to choose from.

![](/posts/pics/Install_Games_from_Epic_Game_Store_and_GOG_with_Heroic_Games_Launcher/heroic_wine_manager.png)

To start off, I suggest you install the latest available version of Proton-GE, which at the time of writing this should be `Proton-GE-Proton7-9`.

Find it in the list and press the download button on the right.

![](/posts/pics/Install_Games_from_Epic_Game_Store_and_GOG_with_Heroic_Games_Launcher/heroic_install_proton.png)

![](/posts/pics/Install_Games_from_Epic_Game_Store_and_GOG_with_Heroic_Games_Launcher/heroic_proton_installing.png)

Now you can navigate to the _Library_ section in the top left corner and install a game.

In the library section you can filter by native Linux games, Windows only games and all games with the filter tool on the top left.

![](/posts/pics/Install_Games_from_Epic_Game_Store_and_GOG_with_Heroic_Games_Launcher/heroic_filter.png)

For this example I'll be installing _realMYST_ from my GOG library. Just click on the game you want to install and press the yellow _INSTALL_ button. It will prompt you to select a folder for installation, as well as a folder to store the _WinePrefix_.

![](/posts/pics/Install_Games_from_Epic_Game_Store_and_GOG_with_Heroic_Games_Launcher/heroic_install_game.png)

![](/posts/pics/Install_Games_from_Epic_Game_Store_and_GOG_with_Heroic_Games_Launcher/heroic_select_folders.png)

Just leave everything as default and press _INSTALL_.

Once the game is installed, you'll have two options: _PLAY NOW_ and _SETTINGS_.

![](/posts/pics/Install_Games_from_Epic_Game_Store_and_GOG_with_Heroic_Games_Launcher/heroic_game_installed.png)

Let's choose _SETTINGS_ and see the options that we have.

![](/posts/pics/Install_Games_from_Epic_Game_Store_and_GOG_with_Heroic_Games_Launcher/heroic_game_settings.png)

From here you can select a variety of advanced options, but the only one I want you to focus on is the _Wine Version_. You can choose the one you've just installed via the _Wine Manager_.

Click on the name of the game at the top of the window to go back, and press _PLAY NOW_.

If the game is supported, it should just launch, and you can finally enjoy your games!

### About compatibility

Needless to say, this whole process isn't supported by either Epic or GOG, so while the vast majority of your games will probably just work, some simply will not.

One glaring example is Fortnite, as [Epic's CEO Tim Sweeney openly declared that they won't enable anticheat support on Linux and the Steam Deck](https://www.gamingonlinux.com/2022/02/epic-games-ceo-says-a-clear-no-to-fortnite-on-steam-deck/).

The same problem exists for any other game from the Epic Store that needs anticheat: while Steam can interface with anticheat software, Heroic can not. This isn't really Heroic's fault, as it is a community project and they've already done a great job allowing Steam Deck and Linux users to play their games. The missing support here is a problem only Epic themselves can address.

Personally I don't think this is much of an issue. You can still use Heroic to play awesome single player games, like the recently released Final Fantasy VII remake.

As for your multiplayer needs, your best bet is Steam, assuming the game you want to play has enabled support for Proton and Steam.

Let me know what you think about this whole situation, and definitely tell me if I've missed anything while covering Heroic.
