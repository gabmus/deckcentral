---
title: "Run Switch Games on Steam Deck With Ryujinx"
date: 2022-03-14T09:46:57+01:00
featured: true
image: "/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/cover.webp"
tags:
  - switch
  - emulator
  - app
  - tutorial
  - ryujinx
---

Want to play your Switch games, but are tired of lugging a crusty old _console_ that can only run games? Then keep reading, and you'll only need to take your Steam Deck with you to enjoy both your PC and your Switch games on the go.

Today we'll be looking at the [Ryujinx Switch emulator](https://ryujinx.org/).

Before we begin, I'd like to quickly go over why I'm choosing to cover Ryujinx instead of the other prominent Switch emulator [Yuzu](https://yuzu-emu.org/).

To put it simply, I've tried both extensively, even going as far as making my own builds. What I can tell you from this experience is that while both are capable emulators, I found Ryujinx's overall quality to be superior compared to Yuzu in terms of stability, ease of use and overall adhesion to code standards.

Your experience might be different, but nothing stops you from trying both and making your own decision. In the meantime, let's take a look at Ryujinx.

### Install Ryujinx

~~It's a little bit more complicated compared to just installing a Flatpak, as unfortunately Ryujinx still hasn't made a Flatpak release, but don't worry, it isn't hard at all.~~

**UPDATE**: Just after 24 hours that I've published this post, I'm proven wrong in the best way possible! **Ryujinx is now available on Flathub**, so if you want to install it via Flatpak and skip this whole section: open up Discover, search for Ryujinx and install it. Alternatively use this terminal command: `flatpak install org.ryujinx.Ryujinx`. If you did, you can skip right to [configuring Ryujinx](#configure-ryujinx).

If you want to install Ryujinx the traditional way, keep reading.

First of all, go to desktop mode, open up Firefox and navigate to [ryujinx.org](https://ryujinx.org).

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/dl_1.png)

Press the _DOWNLOAD_ link at the top.

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/dl_2.png)

Then press the button with the Tux icon in the middle to download the Linux version.

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/dl_3.png)

Select _Save File_, then press ok.

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/dl_4.png)

Open up the downloads menu in the top right, and press the folder icon to navigate to the file we just downloaded. You can now close Firefox.

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/dl_5.png)

Right click on the file you just downloaded (it should be named something like `ryujinx-version-linux_x64.tar.gz`).

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/dl_6.png)

Under _Extract_, select _Extract archive here_. It will extract Ryujinx to a new folder called _publish_.

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/dl_7.png)

To make it easier to find in the future, we can rename this folder to `ryujinx-` followed by the version number. Then move this folder somewhere more accessible. For example, I have a folder in my Home called _Apps_, that's where I keep applications I download this way.

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/dl_8.png)

Open up the Ryujinx folder and you'll see a bunch of files, most of which you don't need to care about. Don't remove them but just leave them there. The important one you're looking for is simply called _Ryujinx_.

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/dl_9.png)

Right click on it and press _Properties_.

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/dl_10.png)

Navigate to the _Permissions_ tab, then enable the checkbox saying _Is executable_.

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/dl_11.png)

Now you're ready to launch Ryujinx.

Just click on the _Ryujinx_ file and it should launch.

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/rj_1.png)

### Configure Ryujinx

At this point unfortunately there isn't much you can do with Ryujinx itself. You'll need to get your hands on a modified Switch to be able to extract its firmware and dump your games.

The easiest way is to dump your game cartridge into an untrimmed XCI file, that should contain both the game and the firmware. I won't detail this process here, but you can easily find proper tutorials on the internet.

If you need to use a _prod.keys_ file, in the menu select _File_, then _Open Ryujunx Folder_. It will open a file manager window. You can place the _prod.keys_ file in the _system_ folder.

UPDATE: I've been told that on Deck _File_ > _Open Ryujinx Folder_ might not work. If that's the case, you can navigate to your Ryujinx folder manually using the Dolphin file manager:

- Open up the Dolphin file manager
- Press _CTRL+H_ if you have a keyboard, alternatively open up the hamburger menu on the top right corner of the window and select _Show Hidden Files_

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/fm_1.png)

- If you installed Ryujinx as a Flatpak, navigate to the following path, starting from your home folder: `.var/app/org.ryujinx.Ryujinx/config/Ryujinx`
- If you installed with the traditional method, instead navigate to this path, starting from your home folder: `.config/Ryujinx`
- You should see the _system_ folder, where you can put your _prod.keys_ file

Once you have extracted your game as an XCI file, put it in your Steam Deck, in a folder where you would ideally collect all of your Switch games. I have a folder called _Switch_ inside my home folder.

Now let's get back to Ryujinx. In the top menu, click _Options_, then _Settings_.

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/rj_2.png)

Press the _Add_ button in the _Game Directories_ section, then select the folder you've put your XCI files into.

Press _Apply_ and then _Save_.

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/rj_3.png)

At this point you should see your games in the main Ryujinx window. Go ahead and double click on it. This will result in a dialog window popping up, asking you to install the firmware from the game you've dumped. Go ahead and press _Yes_, then press _OK_ in the next dialog confirming that the installation was successful.

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/rj_4.png)

If everything went right, the game will launch. Congratulations! But we still need to configure the controls!

To stop the game, in the top menu select _Actions_ and then _Stop Emulation_.

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/rj_5.png)

Select _Options_ from the menu, then _Settings_. Select the _Input_ tab on top and press _Configure_ for _Player 1_.

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/rj_6.png)

In this new window, first select your preferred input device from the _Input Device_ menu on top. You should see your Steam Deck integrated controller in this list.

For the _Controller Type_ menu, I suggest you select _Pro Controller_, as it's the one that most closely matches a regular controller.

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/rj_7.png)

Now you'll have to spend some time configuring all of the button bindings to your preference. Since the Steam Deck has a gyroscope, ~~I suggest you also tick the _Enable Motion Controls_ option, and configure your gyroscope to have all of the input available for the Switch on your Deck~~. UPDATE: I don't have any games that make use of the motion controls, and I assumed it would have just worked. Apparently [the setup is a bit more complicated](https://github.com/Ryujinx/Ryujinx/wiki/Ryujinx-Setup-&-Configuration-Guide#motion-controls), involving using third party tools. I hope to be able to detail this process in the future, but for now you'll have to do without motion controls.

![](/posts/pics/Run_Switch_Games_on_Steam_Deck_with_Ryujinx/rj_8.png)

Once you're satisfied with your bindings, press _Save_, then in the previous window _Apply_ and _Save_.

And that's it! You can now play your Switch games on your Deck!

As usual, let's end this post with a quick note on compatibility: emulators aren't perfect, so you'll probably find some games that don't work properly. To verify your game is compatible, you can check out the [Ryujinx compatibility list page](https://github.com/Ryujinx/Ryujinx-Games-List/issues), search your game and see what you can expect from it.
