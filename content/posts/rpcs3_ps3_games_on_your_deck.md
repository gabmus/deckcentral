---
title: "RPCS3: PS3 Games on Your Deck"
date: 2022-03-19T10:39:30+01:00
image: "/posts/pics/rpcs3_ps3_games_on_your_deck/cover.webp"
tags:
  - app
  - emulator
  - flatpak
  - tutorial
  - ps3
  - sony
  - playstation
  - rpcs3
---

Continuing the trend of emulator tutorials, today we'll look at [RPCS3](https://rpcs3.net/), an emulator for the PlayStation 3.

Let's get started with the installation.

### Install RPCS3

Installing RPCS3 is really easy by using Flatpak. Go to desktop mode, open up the **Discover** app center, search for RPCS3 and install it. Alternatively, you can run this command in the terminal:

```bash
flatpak install net.rpcs3.RPCS3
```

### Configuring RPCS3

Opening up RPCS3, we'll be greeted by this window:

![](/posts/pics/rpcs3_ps3_games_on_your_deck/rpcs3_1.png)

Go ahead and press on the [Quickstart](https://rpcs3.net/quickstart) link either from here or the app window.

After looking at the system requirements, which the Deck meets, we can look at the [Software Requirements section](https://rpcs3.net/quickstart#requirements_software):

![](/posts/pics/rpcs3_ps3_games_on_your_deck/rpcs3_software_requirements.png)

This section will explain that RPCS3 requires the PS3 firmware in order to run. Weirdly enough, Sony themselves allow you to download this firmware directly from their website. So just go ahead and press the _PlayStation 3 System Software_ button on the website, then proceed to download the firmware. You should end up with a file called _PS3UPDAT.PUP_.

Let's get back to the RPCS3 window, tick the box saying _I have read the Quickstart guide_ and press _Continue_.

![](/posts/pics/rpcs3_ps3_games_on_your_deck/rpcs3_2.png)

This window might look slightly different on your system. I had to change the theme in my instance of RPCS3 since the default one looks broken to me. If you are also experiencing this problem, you can easily change your theme as well. In the top menu press _Configuration_ and then select _GUI_.

![](/posts/pics/rpcs3_ps3_games_on_your_deck/rpcs3_3.png)

In the top left corner you'll see a dropdown called _UI Stylesheets_. Choose a theme and press _Apply_.

![](/posts/pics/rpcs3_ps3_games_on_your_deck/rpcs3_4.png)

Once you're done, close the settings window. We'll now install the PS3 firmware. To do so, in the menu press _File_, then _Install Firmware_.

![](/posts/pics/rpcs3_ps3_games_on_your_deck/rpcs3_5.png)

You'll see a file picker dialog. Here choose the _PS3UPDAT.PUP_ file we downloaded earlier. If everything goes well you should see this dialog:

![](/posts/pics/rpcs3_ps3_games_on_your_deck/rpcs3_6.png)

Press _OK_ and another dialog will show up:

![](/posts/pics/rpcs3_ps3_games_on_your_deck/rpcs3_7.png)

Let it finish, then click on the _Pads_ button in the toolbar to configure your controls.

![](/posts/pics/rpcs3_ps3_games_on_your_deck/rpcs3_8.png)

You'll see this window:

![](/posts/pics/rpcs3_ps3_games_on_your_deck/rpcs3_9.png)

In the top left side of the window, use the _Handlers_ dropdown to select an input handler. You should use _Evdev_ for standard XBOX-like controllers and for the Steam Deck integrated controller.

![](/posts/pics/rpcs3_ps3_games_on_your_deck/rpcs3_10.png)

Then use the _Devices_ dropdown in the top middle to select your controller.

![](/posts/pics/rpcs3_ps3_games_on_your_deck/rpcs3_11.png)

Note: if your Steam Deck controller doesn't show up, or if it's acting like a mouse, add RPCS3 to Steam as a non-Steam game and launch it from inside Steam.

Now you can configure your controls. When you're done press _Save_ in the bottom right side of the window.

You should be done with the configuration. Let's move on to running games.

### Running Games

As usual, you'll need to dump your original PS3 games. Fortunately this is easier compared to other consoles, as you just need a standard BluRay drive. Once you've dumped your game, you should have a folder looking like this:

- _PS3_GAME_ (folder)
- _PS3_UPDATE_ (folder)
- _PS3_DISC.SFB_ (file)

Back in RPCS3, open the _File_ menu and select _Boot Game_.

![](/posts/pics/rpcs3_ps3_games_on_your_deck/rpcs3_12.png)

Select the game folder you dumped, and you should see a new window looking similar to this:

![](/posts/pics/rpcs3_ps3_games_on_your_deck/rpcs3_13.png)

Your game will also be added to the internal game list, so that it will be easier to launch in the future.

This window will show a progress bar. During this step, RPCS3 is recompiling the game code to run natively on your system. This can take a while, and it depends on how powerful your CPU is, so just sit tight and wait for it to finish.

Once it's done, it will automatically run the game.

### Updating Games

Depending on the game, you might want to update it to a newer version. To do so you'll need to download the update files (in the correct order) from the PlayStation servers and apply them.

First off you'll need to identify your game's serial number. If you've added the game to your game list, it's right there under the _Serial_ column:

![](/posts/pics/rpcs3_ps3_games_on_your_deck/rpcs3_14.png)

Once you've identified the serial, you'll need to build a URL. Starting from this one:

```
https://a0.ww.np.dl.playstation.net/tpl/np/GAMESERIAL/GAMESERIAL-ver.xml
```

You'll need to replace `GAMESERIAL` with your actual game serial number, in both places. For example if your serial is `ABCD12345`, the correct URL will be: `https://a0.ww.np.dl.playstation.net/tpl/np/ABCD12345/ABCD12345-ver.xml`.

Visit this URL in a browser and you should see an XML file like this:

![](/posts/pics/rpcs3_ps3_games_on_your_deck/update_xml.png)

In each _package_ row of the XML you can see the version of the update file, as well as the URL to download it.

You'll need to download all of the updates between your current version number (shown in RPCS3 in the _Version_ column) and the version you want to update to.

Once you've downloaded all of the updates, you can apply them one by one in RPCS3. To do so, in the _File_ menu select _Install Packages/Raps/Edats_. Then select the first update file. Repeat until you reach the version number you need.

![](/posts/pics/rpcs3_ps3_games_on_your_deck/rpcs3_15.png)

**Note**: you don't always _need_ to update your game to the latest version, and unnecessary updates can result in games not working. Consult the specific compatibility wiki page for the game (more info below) to verify if you do need to update it and to what version.

### Compatibility

Emulators aren't perfect, and emulating a console as complex as the PS3 takes some serious work. You can't expect every game to work, but the compatibility list is already pretty long.

To verify if your game is compatible you can check [RPCS3's compatibility page](https://rpcs3.net/compatibility). Here you can search for a game you want to play, and for each game you can visit its relative wiki page. In these wiki pages you will find extra configurations required for some games to work.

To change the configuration only for a specific game, you can right click on it and select _Create Custom Configuration_. This will take you to a settings window similar to the global one, but the settings you set here will only be applied for the one game you selected.

That should be it! Enjoy your PS3 games, and let me know if I missed anything!
