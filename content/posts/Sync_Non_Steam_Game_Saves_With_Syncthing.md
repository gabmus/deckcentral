---
title: "Sync Non Steam Game Saves With Syncthing"
date: 2022-03-11T00:14:27+01:00
image: "/posts/pics/Sync_Non_Steam_Game_Saves_With_Syncthing/cover.webp"
tags:
  - app
  - network
  - flatpak
  - transfer
  - sync
featured: true
---

The Steam Deck is a full on PC: a beautiful garden, but no walls to speak of. This means you're not locked into playing games from Steam.

There's just one small problem: **you don't get cloud saves for non-Steam games**.

Well, turns out there are alternatives to cloud saves, and today we'll look into one solution in particular that I like: [**Syncthing**](https://syncthing.net/).

Syncthing is a powerful sync program that allows you to keep a select number of folders synced between two or more computers. It even works on Android!

The best part is that it doesn't need any account, as **syncing is done through your local network**.

### Installing Syncthing

Syncthing has a number of different clients you can download depending on the operating system that you use.

On Linux, and of course on the Steam Deck, you can download a client called [**Syncthing-GTK**](https://github.com/kozec/syncthing-gtk). You can install it using Flatpak.

If you're using the Steam Deck or a Linux PC with the Plasma desktop, you can open up the **Discover** app center (you'll need to switch to desktop mode on the Steam Deck), search for **Syncthing GTK** and install it.

If you prefer to use the terminal you can just run:

```bash
flatpak install me.kozec.syncthingtk
```

On Windows, you can download a client called [**SyncTrayzor**](https://github.com/canton7/SyncTrayzor). To install it, [you can follow the instructions provided in SyncTrayzor's GitHub page](https://github.com/canton7/SyncTrayzor#installation). Basically it all boils down to downloading an installer and running it, so it should be pretty straight forward.

### Setting it up

Once you have your Syncthing client open on both of your systems, you'll have to set it up.

Syncthing-GTK walks you through the initial setup process with a **first run wizard**, so let's go through it together.

![](/posts/pics/Sync_Non_Steam_Game_Saves_With_Syncthing/stg_1.png)

In this first screen, you can press _Next_.

![](/posts/pics/Sync_Non_Steam_Game_Saves_With_Syncthing/stg_2.png)

Now it will search for the Syncthing daemon. A daemon is basically a program that runs in the background. In case of Syncthing the daemon is actually the main program, the brains responsible for the app functionality. Since we installed Syncthing-GTK as a Flatpak, it will most certainly find the daemon without any problem, and we can just go ahead and click _Next_.

![](/posts/pics/Sync_Non_Steam_Game_Saves_With_Syncthing/stg_3.png)

The wizard should have now automatically skipped through the _Generate Keys_ section, and will land on the _Setup WebUI_ section. Syncthing can be used directly in its daemon form, by controlling it from a Web UI accessible from your browser. We won't need to worry about this since we're using proper clients, so we'll just select the option saying _Listen on localhost_ and once again press _Next_

![](/posts/pics/Sync_Non_Steam_Game_Saves_With_Syncthing/stg_4.png)

That's it! The wizard should have skipped through _Saving Settings_, and you should see the _Finish_ section. Just go ahead and press _Close_.

![](/posts/pics/Sync_Non_Steam_Game_Saves_With_Syncthing/stg_5.png)

The first thing it's gonna ask is to allow usage reporting. You can decide to press either _Yes_ or _No_ depending on your preference. This allows the developers to gather data on how you use Syncthing, but it won't affect your experience, so it's really just your preference.

![](/posts/pics/Sync_Non_Steam_Game_Saves_With_Syncthing/stg_6.png)

This is the main window you'll need to familiarize with.

On the left side of the window you can see a list of synced folders (at first you should only see one folder called _Default Folder_). Each item in this list can be expanded by clicking on it, but we'll get to this later.

On the right side of the window you can see a list of devices connected to your Syncthing network. Initially you'll only see your current device, with some statistics that you can keep an eye on.

On the top right corner you can see a button with a cog icon. This will open the main application menu.

![](/posts/pics/Sync_Non_Steam_Game_Saves_With_Syncthing/stg_7.png)

The first thing we're going to do is open the menu by clicking the button with the cog icon, then select the _Daemon Settings_ option.

![](/posts/pics/Sync_Non_Steam_Game_Saves_With_Syncthing/stg_8.png)

Since we're going to use Syncthing on the local network only, go ahead and disable the option saying _Enable Global Discovery_.

You can also disable _Open browser with WebUI when daemon is starting_, since we're not going to use the Web UI.

Click on _Save_.

Now you'll need to repeat this initial configuration process on your other computer, and you can use the same options I described here.

Since the following process can easily get confusing, I'll name the two devices you want to sync **device A** and **device B**. It doesn't matter which is which: your Deck can be A and your PC can be B, or vice-versa, the process will be the same.

![](/posts/pics/Sync_Non_Steam_Game_Saves_With_Syncthing/stg_9.png)

In **device A** open the cog menu and click _Show ID_. This will open a window showing the ID as a string you can copy and as a QR code. You'll need to copy this ID to **device B**. You can either copy it by hand, or send it to yourself via e-mail or using a chat of your preference.

In **device B** open the cog menu, then select the _Add Device_ option.

![](/posts/pics/Sync_Non_Steam_Game_Saves_With_Syncthing/stg_10.png)

Still in **device B**, insert **device A**'s ID in the _Device ID_ text box.

You can optionally insert a name of your preference in the _Device Name_ text box. This isn't required, but will make your devices easier to recognize.

Enable the only folder you'll see in the _Share Folders_ section. This will enable sharing the default folder between the two devices.

Finally click _Save_.

![](/posts/pics/Sync_Non_Steam_Game_Saves_With_Syncthing/stg_11.png)

Wait a little bit and on **device A** you should see a notification show up, telling you that **device B** wants to connect. Click on the _Add_ button.

This will show a window you should already be familiar with: it's the same we've seen on **device B** a while ago. Keep everything as is and enable the only folder in the _Share Folder_ section. Click _Save_.

At this point you'll have one folder synced between the two devices. We'll keep this folder, as it's useful for syncing miscellaneous files between the two devices.

But the objective of this tutorial is syncing game files for non-Steam games, so let's see how to do that.

The first thing you want to make sure of is that you've run the game at least once on both deviecs. This will make sure you've generated the save folders in both devices.

The next step is figuring out where the saves for your particular game are stored. This varies depending on the game and the operating system, so you'll need to do your research before continuing with this tutorial.

Once you find your **save folder**, you can add it to Syncthing, but **before doing that**:

- Make sure the game is closed on both devices.
- **Make a backup!** Seriously, if you do something wrong you could lose your saves! That's no fun, so copy your save folder to another location so that if anything goes wrong, you can just restore this backup.
- Delete the contents of the save folder on the receiving device. This means, if you want to sync your emulator saves **from your PC to your Deck**, remove the contents of the emulator save folder on the Deck. This will be necessary to avoid having your old save files overwritten by brand new ones with no progress.

Now, to add your save folder to Syncthing, on **device B** open up the cog menu and click on _Add Shared Folder_.

![](/posts/pics/Sync_Non_Steam_Game_Saves_With_Syncthing/stg_12.png)

- In _Folder Label_ you can insert a name of your preference, like the name of the game.
- _Folder ID_ is the unique identifier for the folder you want to sync. It needs to be the same on both devices. You can keep the randomly generated ID Syncthing automatically generates, or you can put in the name of the game to make it easier.
- Click on the _Browse..._ button and select the save folder you've found earlier
- You can set a rescan interval of your preference. This is the time interval that Syncthing will wait before checking if there are any changes that need to be synced. Alternatively you can enable the _Monitor filesystem for changes_ option to let Syncthing do some filesystem magic and be more responsive with change detection. This might not work as expected depending on your system, so if you're not sure, keep it disabled.
- Disable both _Send Only Folder_ (should be already disabled) and _Receive Only Folder_.
- Click on the _Share With Devices_ tab

![](/posts/pics/Sync_Non_Steam_Game_Saves_With_Syncthing/stg_13.png)

- Enable the only device you see here (should be **device A**)
- Click _Save_

After a little bit, on **device A** you'll see a notification saying that **device B** wants to share a new folder.

![](/posts/pics/Sync_Non_Steam_Game_Saves_With_Syncthing/stg_14.png)

Go ahead and click _Add_.

This will be the same window we've seen before on **device B**, so go ahead and configure it the same way. Again, select the path to your save folder and make sure to disable both _Send Only Folder_ and _Receive Only Folder_.

Then go ahead and click _Save_.

And that should be it! If you did everything right, your saves should be syncing automatically between the two devices.

A couple of finishing notes:

- Syncthing can be a bit fickle sometimes, so if you think something is taking a bit too long, you can open up the cog menu and click on _Restart Daemon_. This should be enough to let it catch up on changes.
- It might be a good idea to just open Syncthing when you need to sync your saves, and close it when you're done. If you simply close the window, Syncthing will remain active in the background. To avoid that, instead of just closing the window, open the cog menu and select _Shutdown Daemon_. This will show a dialog window saying _Syncthing has been shut down_. Go ahead and press _Quit_.
