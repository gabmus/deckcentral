---
title: "Play Retro Games on Steam Deck With RetroArch"
date: 2022-03-18T10:49:33+01:00
image: "/posts/pics/play_retro_games_with_retroarch/cover.webp"
featured: true
tags:
  - emulator
  - app
  - flatpak
  - tutorial
  - retroarch
---

Retro gaming is hugely popular these days, and playing your old games on the go on the Steam Deck is a very attractive idea.

Today we'll take a look at [RetroArch](https://www.retroarch.com/), an all in one solution for emulating your old games from different consoles of yesteryear.

### Install RetroArch

RetroArch can be installed directly through Steam, although from what I understand the version that's on Steam is not on par with other releases.

For this reason we'll go in a different direction and instead install it through Flatpak.

To do so, you'll need to go to desktop mode, then open up the **Discover** app center, search for _RetroArch_ and finally install it. Alternatively you can use this terminal command:

```bash
flatpak install org.libretro.RetroArch
```

### Configuring RetroArch

Open up RetroArch, you should see the interface right away. Depending on your configuration your controller (either built-in or external) should be picked up right away and you should be able to use it to navigate the interface.

![](/posts/pics/play_retro_games_with_retroarch/ra_1.png)

Let's make a few configuration changes before moving on.

If your controller is not picked up, or if you want to change some controls from the default (for example on an XBOX style controller like the one on the Steam Deck, RetroArch uses the **B** button as _OK_ and the **A** button as _Back_ or _Cancel_), you'll want to mess with the controller configuration.

If the controller doesn't work at all you can interact with the interface using a mouse or via the touch screen.

Another small note, if your controller still behaves like a mouse (since you are in desktop mode), you might want to add RetroArch as a non-Steam game and open it up through Steam.

To change the controls you'll need to navigate to _Settings_ and then _Input_.

![](/posts/pics/play_retro_games_with_retroarch/ra_2.png)

To swap the _OK_ and _Cancel_ buttons, select _Menu Controls_ and enable _Menu Swap OK and Cancel Buttons_

![](/posts/pics/play_retro_games_with_retroarch/ra_2_1.png)

![](/posts/pics/play_retro_games_with_retroarch/ra_2_2.png)

Now you'll want to enable a shortcut for opening the menu while you are in game. To do so, go back and select _Hotkeys_.

![](/posts/pics/play_retro_games_with_retroarch/ra_2_3.png)

Select _Menu Toggle Controller Combo_ and set it to a shortcut of your preference. I use _L3 + R3_ (pressing down on both analog sticks), but I can also recommend _Start + Select_.

![](/posts/pics/play_retro_games_with_retroarch/ra_2_4.png)

Go back and scroll all the way down to _Port 1 Controls_ and select it. 

![](/posts/pics/play_retro_games_with_retroarch/ra_3.png)

Here you'll be able to change various options, which should be fairly easy. The only important setting I want to detail that may not be immediately easy to understand is _Analog to Digital Type_: this allows you to use one of the analog sticks to emulate the D-Pad input for consoles that didn't have an analog stick. I recommend you set it to _Left Analog_.

![](/posts/pics/play_retro_games_with_retroarch/ra_4.png)

![](/posts/pics/play_retro_games_with_retroarch/ra_5.png)

Next, you should probably enable fullscreen mode for the best experience. To do so, go back to _Settings_ and select _Video_.

![](/posts/pics/play_retro_games_with_retroarch/ra_6.png)

![](/posts/pics/play_retro_games_with_retroarch/ra_7.png)

Here select _Fullscreen Mode_. This should immediately switch to fullscreen mode.

![](/posts/pics/play_retro_games_with_retroarch/ra_8.png)

As far as configuration goes, we should be done.

### Installing Cores

RetroArch itself isn't an emulator. Instead it uses existing emulators to run your games. These are called _Cores_, and there are many different ones for various retro consoles you can choose from.

To install cores, you need to go back to the main menu and select _Load Core_, then _Download a Core_.

![](/posts/pics/play_retro_games_with_retroarch/ra_9.png)

There is a huge number of cores you can download here, depending on the game you want to play. Here are the ones that I downloaded:

- Nintendo - Game Boy Advance (mGBA)
- Sega - Dreamcast/NAOMI (Flycast)
- Sony - PlayStation (Beetle PSX HW)
- Sony - PlayStation 2 (PCSX2)
- Sony - PlayStation Portable (PPSSPP)

Note that depending on the particular console you're trying to emulate you might need a BIOS file. You can extract them from your original consoles, but if you lack the tools to do so or your console is broken, you can try looking them up on the internet. Note that this is a legal gray area, so do so at your own risk.

To learn more about installing BIOSes you can reference [Libretro's user documentation](https://docs.libretro.com/library/bios/).

### Importing games

You'll need to extract the ROM files from the original games. It might be easier for certain consoles than others, depending on the system you want to emulate. Again, if you lack the tools to dump your own games, you can download them from the internet. This as well might be illegal so proceed with caution.

Once you have all of your game ROMs, it's a good idea to categorize them neatly in a dedicated folder. This is how I organize my games:

- Home
  - Games
    - Emulators
      - GBA
      - PS1
      - PS2
      - ...

You can easily organize your ROM files using the Dolphin file manager included in your Steam Deck.

Once you're done, go back to RetroArch and go to _Import Content_, then select _Scan Directory_.

![](/posts/pics/play_retro_games_with_retroarch/ra_10.png)

Select your home folder (should be `/home/yourusername`).

![](/posts/pics/play_retro_games_with_retroarch/ra_11.png)

Navigate to the folder containing your game ROMs, then select _\<Scan This Directory\>_

![](/posts/pics/play_retro_games_with_retroarch/ra_12.png)

Once it's done, go back to the main menu, and you should see a list of consoles at the very bottom of the sidebar.

![](/posts/pics/play_retro_games_with_retroarch/ra_13.png)

Select the game you want to play and press _Run_, then select the _Core_ you want to use (the list should only contain cores that can actually run the game), finally press _Run_ again. This should start the game and you'll be able to finally go ahead and play!

To open up the emulator menu you can use the shortcut we set up earlier, or press **F1** if you have a keyboard connected.

In this menu you'll be able to close your game, save and restore the state and change some settings specific to the game you're playing, as well as playing around with shaders to get some more visual flare out of your old games (this is an advanced option, be careful).

That should be it! Let me know your thoughts, and if I missed anything in this guide, down in the comments.

UPDATE: I've been asked how to use shaders to add extra flare to your games. This might seem a bit complicated, but it's actually a fairly easy process if you use shader presets. You can find a guide in the [Libretro's documentation](https://docs.libretro.com/shader/introduction/).
