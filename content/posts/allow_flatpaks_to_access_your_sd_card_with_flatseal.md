---
title: "Allow Flatpaks to Access Your SD Card With Flatseal"
date: 2022-03-31T10:01:28+02:00
image: "/posts/pics/allow_flatpaks_to_access_your_sd_card_with_flatseal/cover.webp"
tags:
  - flatpak
  - app
  - tutorial
  - sandbox
---

Flatpak is the main way to install apps on your Steam Deck. It's an awesome system and there are tons of apps for you to install!

The problem is that Flatpak sandboxes your apps, using a fine-grained permission system, similar but much more advanced than what you might see on your smartphone OS.

Because of this, Flatpak apps might not have access to some resources on your Steam Deck. The most obvious example is an emulator like [RetroArch](/posts/play_retro_games_with_retroarch/) or a game launcher like [Heroic](/posts/install_games_from_epic_game_store_and_gog_with_heroic_games_launcher/) **not being able to access your SD card**. Don't worry: there is a way to enable whatever permission you might need with the help of a simple app called [Flatseal](https://github.com/tchx84/flatseal).

### Install Flatseal

If you've installed other Flatpak apps already, this should be familiar: go to desktop mode, open up the **Discover** app center, search for _Flatseal_ and install it.

Alternatively, from the terminal:

```bash
flatpak install com.github.tchx84.Flatseal
```

### How to use it

Before starting up Flatseal, make sure the app you want to modify the permissions for is closed.

Once you open up Flatseal you'll see the main window, showing a list of your apps on the left, and the permissions for the selected app on the right.

![](/posts/pics/allow_flatpaks_to_access_your_sd_card_with_flatseal/fs_1.png)

In the top right corner of the window you'll notice a button labeled _Reset_. If you mess up anything while tinkering, select your app and press this Reset button, it will revert its permissions to the default values.

Let's get started by selecting an app, for instance _RetroArch_.

There are lots of permissions here, and for this tutorial you shouldn't care about most of them. What you should care about is the section labeled _Filesystem_. Scroll down in the right view until you find it.

To allow apps to read your SD card, as well as other external storage you might attach, like external drives, all you should need to do is give them permission to _read and write_ the following path: `/run/media`.

To do so, in _Other files_, click on the _Add folder_ icon on the right.

![](/posts/pics/allow_flatpaks_to_access_your_sd_card_with_flatseal/fs_2.png)

This will create a new row with a text entry, showing a yellow danger icon (similar to this: ⚠).

![](/posts/pics/allow_flatpaks_to_access_your_sd_card_with_flatseal/fs_3.png)

In this text entry, you should enter the following: `/run/media:rw`.

This allows the app to read and write to the `/run/media` folder, a system folder where your external drives are mounted.

Now you can open up your app, and you should be able to use your external drives like your SD card like normal. Let's see a couple of examples. As an external drive I'll be using a USB thumb drive called _CARRY_, the same applies for your Steam Deck SD card.

#### Example 1: RetroArch

I'll put some ROMs on my external drive and access them from RetroArch. Once I set up the permission with Flatseal, I just open it up and select _Import Content_, then _Scan Directory_.

![](/posts/pics/allow_flatpaks_to_access_your_sd_card_with_flatseal/ex_ra_1.png)

I can immediately see a list of directories RetroArch can access, among those there's one called `/run/media/gabmus`.

![](/posts/pics/allow_flatpaks_to_access_your_sd_card_with_flatseal/ex_ra_2.png)

In your particular case, you'll see a folder called `/run/media` followed by your username. Select it.

![](/posts/pics/allow_flatpaks_to_access_your_sd_card_with_flatseal/ex_ra_3.png)

As you can see my thumb drive called _CARRY_ is listed right there. I can select it and then select _\<Scan This Directory\>_ to add any games I have in this drive to my RetroArch.

Let's move on to another example: the Heroic Games Launcher

#### Example 2: Heroic Games Launcher

I want to set my external drive as the default installation folder in Heroic. This way I can store my games there and save space in my internal drive.

Opening up Heroic, go to _Settings_, then look at the entry labeled _Default Installation Path_. Click on the folder icon on the right side of the entry.

![](/posts/pics/allow_flatpaks_to_access_your_sd_card_with_flatseal/ex_hgl_1.png)

This will open up a file selector dialog. You should see your external drive in the sidebar. Select it and you should be good to go.

---

That should be it for this tutorial, hopefully it will help you. If I missed anything or if something isn't working right, make sure to let me know and I'll try my best to figure it out and update this article.
