---
title: "How to Setup Proton GE"
date: 2022-03-10T07:53:20+01:00
image: "/posts/pics/How_to_setup_Proton_GE/cover.webp"
tags:
  - proton
  - wine
  - protonup
  - flatpak
---

Proton is the secret sauce that makes the Steam Deck so great.

Proton is a distribution of **Wine**, Wine is a recursive acronym that stands for _Wine is not an emulator_. It's a compatibility layer that implements the Windows API to work on a Linux system.

To put it simply, Wine &mdash; and consequently Proton &mdash; lets you run Windows only games on a Linux PC, and this includes the Steam Deck!

Valve themselves offer a variety of Proton versions, with some versions working better for certain games.

But they're not the only ones putting out Proton releases.

Red Hat engineer and community legend [Glorious Eggroll](https://www.gloriouseggroll.tv/) develops their own distribution of Proton, aptly named **Proton GE** (where GE stands for "Glorious Eggroll" in case you were wondering).

This particular version of Proton is faster moving compared to Valve's own, plus it's usually much closer to the latest releases of upstream Wine, and it includes a bunch of bleeding edge features and fixes that usually end up in Valve's official Proton releases, just after a while.

If a particular game is giving you trouble, either acting up or not working at all, it's probably a good idea to try launching it with Proton GE.

### How to install it

To install Proton GE and many other compatibility layers (that I'll hopefully be able to cover in the close future), the easiest solution is using a tool developed by community member [DavidoTek](https://davidotek.github.io/) called **ProtonUp-Qt**.

To install ProtonUp-Qt on your Steam Deck, you want to resort to using **flatpak**. Simply go to desktop mode, open up the **Discover** app center and search for **ProtonUp-Qt**.

If you prefer installing it from the terminal, you will just have to run:

```bash
flatpak install net.davidotek.pupgui2
```

Once you have it installed, run it as you would any other app.

![](/posts/pics/How_to_setup_Proton_GE/pupgui2-dark.webp)

For the _Install for_ dropdown, choose Steam, then click on _Add version_. This will open up another window, where you can select _Proton-GE_ as the compatibility tool, and usually you want to select the latest version.

Finally click on Install and... you're done!

Give your Steam Deck a reboot for good measure and you're ready to use Proton-GE.

### How to use it

- Open the game (don't start it, just tap on it)
- Tap on the cog all the way to the right
- Tap on _Properties..._
- Select _Compatibility_ on the left
- Tick the option that says _Force the use of a specific Steam Play compatibility tool_
- Then press on the dropdown that should show up just below the tick
- Finally select the version of Proton-GE you just installed

And you can follow the same procedure to change the proton version to any one you prefer. Maybe Proton-GE isn't working for you and you want to switch back to another version. Follow the same procedure and try out whichever Proton version you see fit.

And here are screenshots showing the process I just described:

![](/posts/pics/How_to_setup_Proton_GE/deckui_1.png)

![](/posts/pics/How_to_setup_Proton_GE/deckui_2.png)

![](/posts/pics/How_to_setup_Proton_GE/deckui_3.png)

![](/posts/pics/How_to_setup_Proton_GE/deckui_4.png)
