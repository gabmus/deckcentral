---
title: "Moving Files To And From the Deck"
description: "How to transfer files to and from your Steam Deck from your PC or smartphone"
date: 2022-03-09T15:20:44+01:00
image: "/posts/pics/Moving_Files_To_And_From_the_Deck/cover.webp"
tags:
  - file
  - transfer
  - connect
  - network
---

One of the most popular questions about the Steam Deck is how to move files to and from it from another computer.

We're used to connecting our smartphones and tablets via USB and having them accessible as if they were standard thumb drives, and many users find it strange that that's not how the Steam Deck works.

But the Steam Deck isn't a phone or a tablet, it's not even really a console. It's a full-fledged computer, not unlike a traditional laptop or desktop PC.

This said, while using a regular USB cable won't work, there are many different ways to transfer files to and from your Steam Deck. Here are a few.

### Warpinator

Warpinator is a program that you can install on the Steam Deck, as well as on your Linux or Windows PC, to move files across a local network. It's probably the easiest and most effective solution out there.

This was initially suggested by Liam from GamingOnLinux, and I can only second his suggestion. To learn more, [go check out the Warpinator tutorial on the GamingOnLinux website](https://www.gamingonlinux.com/2022/03/heres-how-to-transfer-files-from-your-pc-to-a-steam-deck/).

### Using an external drive

This is the old way of doing things, a bit traditional, and possibly not the fastest option, but it works, and you don't need to install anything. Just use a USB type C dongle on your Deck to connect a USB thumb drive or another type of external storage medium, then transfer all the files you want!

Any external storage medium should be available from the Dolphin file manager, in desktop mode. **Just make sure that both your Deck and your PC can access the drive**. For best compatibility your external drive should be formatted in either _exFAT_, _FAT32_ or _NTFS_ in order from most to least appropriate.

### From a smartphone

If you have a smartphone that can be connected via USB to a PC to transfer files, you can do the same thing with a Steam Deck. You may need to use a dongle here as well, depending on if you have a type C to type C cable or not. It should just show up in desktop mode just as if you were using a thumb drive.
