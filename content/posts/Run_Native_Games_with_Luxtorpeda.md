---
title: "Run Native Games With Luxtorpeda"
date: 2022-03-10T12:25:08+01:00
tags:
  - protonup
  - luxtorpeda
  - engine
  - native
  - tutorial
image: "/posts/pics/Run_Native_Games_with_Luxtorpeda/cover.webp"
---

There are many games out there, especially older ones, that are using game engines that have been either reimplemented from scratch by the community using modern technologies, or straight up available natively for Linux. Some notable mentions you might have heard of include GZDoom, OpenMW, ScummVM and eduke32.

These "alternative engines" are capable of bringing ancient relics into the current era and improving compatibility or performance for games that you would otherwise only be able to run through Proton; plus this usually comes with engine bug fixes and sometimes even extra features like proper widescreen patches and broader modding support.

Although running your games using these game engines can be a bit hard &mdash; often involving extracting files from the original game and messing with custom configs &mdash; once again the community comes to the rescue, and thanks to the [Luxtorpeda project](https://luxtorpeda-dev.github.io/) you can easily use them directly from your Steam library with a lot less effort.

Luxtorpeda is a compatibility tool using the same infrastructure Proton uses, except, instead of using a version of Wine to run games, it downloads and uses a third party game engine (given the game is supported).

If you've read [my previous article on how to setup Proton-GE](/posts/how_to_setup_proton_ge/), then you're already familiar with the process to install Luxtorpeda.

To summarize:

- Go to desktop mode
- Open up the **Discover** app center
- Search for an app called **ProtonUp-Qt** and install it.

Alternatively you can install it from the terminal by running:

```bash
flatpak install net.davidotek.pupgui2
```

![](/posts/pics/How_to_setup_Proton_GE/pupgui2-dark.webp)

- Open it up and select your Steam install in the _Install for_ dropdown
- Click on the _Add version_ button, this will create another window
- Select _Luxtorpeda_ in the _Compatibility tool_ dropdown
- Select the latest version in the _Version_ dropdown
- Click _Install_ and you're done.

You might need to restart Steam, or just reboot the entire system for good measure.

Now comes the fun part, and that's of course playing some games!

The important thing to keep in mind with Luxtorpeda is that individual games need to be supported, so you'll need to manually [sift through the list of supported games on the Luxtorpeda website](https://luxtorpeda-dev.github.io/) and make sure the game you want to play is in there (if it's not, you can try running it with Proton).

Once you're sure, you can just follow [the same procedure I showed in the Proton-GE post](/posts/how_to_setup_proton_ge/#how-to-use-it), but instead of selecting a Proton version, select Luxtorpeda. Depending on the particular game you want to play, you might need to follow some extra steps: you can find all of the info you need on the [Luxtorpeda website](https://luxtorpeda-dev.github.io/).

With that said, just like Proton, Luxtorpeda isn't a _silver bullet_: some games will run better, some won't run at all, and some will have an incomplete feature set.

It's really something you'll need to play around and experiment with, but for the vast majority of supported games I don't think you will have much of a problem.

Wrapping up, here's the link to the [Luxtorpeda GitHub page](https://github.com/luxtorpeda-dev/luxtorpeda) to learn more about it.

If you have any questions, or if you're having a hard time with a particular game you're trying to run, feel free to leave a comment below.
