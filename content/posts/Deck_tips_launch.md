---
title: "Deck Central Launch"
date: 2022-03-09T14:30:23+01:00
---

The Steam Deck is undoubtedly one of the greatest revolutions in the gaming space in the last 20 years or more.

One of the biggest difference from mainstream PCs and existing consoles is that the Steam Deck ships with Linux, opening up the door for infinite potential for this small but powerful device.

I decided to create this website, _Deck Central_, to share tips, tricks, tutorials, guides and any other knowledge that Deck users will find useful, not just for gaming, but also to make the best use of what is, in fact, a fully featured computer in console's clothing.
