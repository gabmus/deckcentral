---
title: "Get Your Deck's IP Address"
description: "3 ways to easily get your Steam Deck's IP address"
date: 2022-03-09T15:22:22+01:00
image: "/posts/pics/Get_Your_Decks_IP/cover.webp"
tags:
  - network
  - ip
  - terminal
  - kde
---

If you need to interact with your Steam Deck from another PC, you'll likely need to know its IP address.

Fortunately this is fairly easy, and there are a number of different ways to get this information.

### 1. From the Deck UI settings

- Open up the menu
- Go to Settings
- Open the Internet section
- Tap on your active network
- And here's your IP

You can follow these steps with the help of the following screenshots:

![](/posts/pics/Get_Your_Decks_IP/deckui_1.png)

![](/posts/pics/Get_Your_Decks_IP/deckui_2.png)

![](/posts/pics/Get_Your_Decks_IP/deckui_3.png)

![](/posts/pics/Get_Your_Decks_IP/deckui_4.png)

### 2. From the KDE (desktop mode) tray

If you need to know your IP when you're in desktop mode, follow this procedure:

- Click on the system tray network icon in the bottom right corner of the screen
- Click on your active connection
- Click on details
- Your IP address will be marked as `IPv4 Address`

Here are some screenshots detailing the process:

![](/posts/pics/Get_Your_Decks_IP/kde_1.png)

![](/posts/pics/Get_Your_Decks_IP/kde_2.png)

![](/posts/pics/Get_Your_Decks_IP/kde_3.png)

### 3. From the terminal

If you don't feel like using any of the above options, there's a very simple &mdash; albeit not necessarily intuitive &mdash; way to get your IP using the terminal.

You'll need to go to desktop mode, then open up a terminal app, so most likely **Konsole**, then type:

```bash
ip address
```

Just for your interest, you can also type `ip addr` or even `ip a` and get the same result. The command is quite intuitive and easy to remember, but its output isn't too easy to parse if you don't know what you're looking at.

To make it a bit easier to read, you can instead add the `-c` option, which stands for _color_. So your command would look like this: `ip -c address`.

Let's now have a look at the output so that you can understand what you're looking at.

![](/posts/pics/Get_Your_Decks_IP/ip_addr_out.png)

That's a lot easier to read now.

As you can see, the output consists of a list of items, each item is a _network interface_.

Note that your particular output can be different from mine.

The first network interface is `lo`, also known as _loopback_. This is a virtual network interface connecting your Deck to itself. **You can ignore this**.

The second network interface here starts with `enp`, this is an (inactive) wired ethernet connection. You're most likely connecting your Deck to your network using Wi-Fi, so you need to look at the third and last connection, the one that starts with `wlp`. **This is your wireless network interface**, and that's what you're looking for.

There's a lot of information you don't need here, so just look at the row that says `inet`. In purple you can see a bunch of numbers, separated by periods and ending with a `/24`.

Your IP address is the one you see in purple, and can change depending on the network you're connected to. In this case, my IP is `192.168.1.54`.
