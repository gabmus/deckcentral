---
title: "Other Resources"
norss: true
nosearch: true
showDate: false
---

Here are some other awesome websites and YouTube channels you should check out:

- [GamingOnLinux](https://www.gamingonlinux.com/): Linux Gaming, SteamOS and Steam Deck gaming. Covering Linux Gaming News, Linux Games, SteamOS, Indie Game Reviews, Steam Play Proton and more.
- [Linux Gaming Central](https://linuxgamingcentral.com/): Your portal to everything Linux.
- [Open For Everyone](https://openforeveryone.net/): Fun, approachable coverage of Linux distributions, hardware, open-source apps, and Valve's Steam Deck. Brought to you by Jason Evangelho.
- [Gardiner Bryant](https://www.youtube.com/c/GardinerBryant): Weekly content revolving around Tech, Open Source Software, and Linux gaming news.
